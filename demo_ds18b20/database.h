/********************************************************************************
 *      Copyright:  (C) 2021 Li Guangming
 *                  All rights reserved.
 *
 *       Filename:  database.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(19/05/21)
 *         Author:  Li Guangming <2360217827@qq.com>
 *      ChangeLog:  1, Release initial version on "19/05/21 09:05:05"
 *                 
 ********************************************************************************/
#ifndef _DATABASE_H_
#define _DATABASE_H_

int database_init(sqlite3 **db,char* db_file);
int save_data(sqlite3 *db,float *tmp,char *device_id);
int get_nrow(sqlite3 *db, int *number);
int get_data(sqlite3 *db,float* tmp,char *device_id);
int get_rowid(sqlite3 *db);
int delete_data(sqlite3 *db,int rowid);

#endif
