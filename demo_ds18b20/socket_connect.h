/********************************************************************************
 *      Copyright:  (C) 2021 Li Guangming
 *                  All rights reserved.
 *
 *       Filename:  socket_connect.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(19/05/21)
 *         Author:  Li Guangming <2360217827@qq.com>
 *      ChangeLog:  1, Release initial version on "19/05/21 10:17:00"
 *                 
 *******************************************************************************/
#ifndef  _SOCKET_CONNECT_H_
#define  _SOCKET_CONNECT_H_


int socket_connect(char *serverip, int port);

#endif

