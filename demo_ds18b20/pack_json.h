/********************************************************************************
 *      Copyright:  (C) 2021 Li Guangming
 *                  All rights reserved.
 *
 *       Filename:  pack_json.h
 *    Description:  This head file i
 *
 *        Version:  1.0.0(19/05/21)
 *         Author:  Li Guangming <2360217827@qq.com>
 *      ChangeLog:  1, Release initial version on "19/05/21 08:13:35"
 *                 
 ********************************************************************************/

#ifndef _PACK_JSON_H_
#define _PACK_JSON_H_

int pack_json(char *buf, int size, char *id, float tmp);

#endif
