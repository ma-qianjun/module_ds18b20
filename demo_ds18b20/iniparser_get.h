/********************************************************************************
 *      Copyright:  (C) 2021 Li Guangming
 *                  All rights reserved.
 *
 *       Filename:  iniparser_get.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(19/05/21)
 *         Author:  Li Guangming <2360217827@qq.com>
 *      ChangeLog:  1, Release initial version on "19/05/21 08:11:08"
 *                 
 ********************************************************************************/

#ifndef _INIPARSER_GET_H_
#define _INIPARSER_GET_H_

int iniparser_get(char *conf_file,int *port,char *serverip,int size);

#endif

