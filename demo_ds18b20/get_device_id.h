/*********************************************************************************
 *      Copyright:  (C) 2021 Li Guangming
 *                  All rights reserved.
 *
 *       Filename:  get_device_id.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(19/05/21)
 *         Author:  Li Guangming <2360217827@qq.com>
 *      ChangeLog:  1, Release initial version on "19/05/21 11:32:15"
 *                 
 ********************************************************************************/

#ifndef _GET_DEVICE_ID_H_
#define _GET_DEVICE_ID_H_

int  get_device_id(char *id, int size);



#endif
