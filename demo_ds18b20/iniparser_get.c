#include<stdio.h>
#include<string.h>

#include"iniparser_get.h"
#include"iniparser.h"
#include"dictionary.h"


int iniparser_get(char *conf_file,int *port,char *serverip,int size)
{


    conf_file = "./client_server.ini";

    const char* str;
    int     ret;

    dictionary *dic=NULL;

    dic = iniparser_load("./client_server.ini");

    str = iniparser_getstring(dic,"address:serverip","0");//第二个参数是const char * ,所以要const char* str;
    strncpy(serverip, str, size);

    ret = iniparser_getint(dic,"address:port",0);
    *port = ret;


    return 0;

}

