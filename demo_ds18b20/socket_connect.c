/*********************************************************************************
 *      Copyright:  (C) 2021 Li Guangming
 *                  All rights reserved.
 *
 *       Filename:  socket_connect.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(19/05/21)
 *         Author:  Li Guangming <2360217827@qq.com>
 *      ChangeLog:  1, Release initial version on "19/05/21 10:08:31"
 *                 
 ********************************************************************************/
#include<stdio.h>
#include<errno.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include <stdlib.h>
#include <netdb.h>
#include <dirent.h>
#include <sys/wait.h>
#include <signal.h>
#include <stddef.h>

#include"socket_connect.h"


int socket_connect(char *serverip, int port)
{

    int                   connfd = -1;
    struct sockaddr_in    serv_addr;

    connfd = socket(AF_INET,SOCK_STREAM,0);
    if (connfd < 0)
    {
    //    printf("create socket failure:%s\n",strerror(errno));
        return -2;
    }
    
    memset(&serv_addr,0,sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);
    inet_aton(serverip,&serv_addr.sin_addr);

    if (connect(connfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
    {
      //  printf("connect to server [%s:%d] failure :%s\n", serverip, port, strerror(errno));
        return -3;
    }

    return connfd;


}
