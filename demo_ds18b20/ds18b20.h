/********************************************************************************
 *      Copyright:  (C) 2021 Li Guangming
 *                  All rights reserved.
 *
 *       Filename:  ds18b20.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(19/05/21)
 *         Author:  Li Guangming <2360217827@qq.com>
 *      ChangeLog:  1, Release initial version on "19/05/21 09:58:11"
 *                 
 ********************************************************************************/
#ifndef _DS18B20_H_
#define _DS18B20_H_

int ds18b20(float *tmp);

#endif 
