#include<stdio.h>
#include<string.h>
#include "cJSON.h"
#include"pack_json.h"


int pack_json(char *buf, int size, char *id, float tmp)
{
    cJSON *root = NULL;
    if (!buf ||size <= 0 || !id)
    {
        printf("Invalid input arguments\n");
        return -1;
    }

    root = cJSON_CreateObject();

    cJSON_AddStringToObject(root, "id", id);
    cJSON_AddNumberToObject(root, "temperature", tmp);
//    cJSON_AddStringToObject(root, "get_time", buf_time);

    strncpy(buf,cJSON_Print(root),size);
    //  printf("%s\n",cJSON_Print(root));
    cJSON_Delete(root);
    return 0;
}
