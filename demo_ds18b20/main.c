#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<dirent.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<time.h>
#include<sqlite3.h>
#include<signal.h>


#include"socket_connect.h"
#include"ds18b20.h"
#include"iniparser_get.h"
#include"pack_json.h"
#include"database.h"
#include"get_device_id.h"

int     sig_stop = 0;

void signal_stop(int signum)
{

    if (signum == SIGINT)
    sig_stop = 1;

}

int main(int argc, char **argv)
{
    sqlite3      *db;

    int           port = 0;
    int           connfd = -1;
    int           rowid = 1;
    int           rv = -1;
    int           ret = -1; 
    int           len;
    int           number[1024];
    int           i;
        
    float         tmp;
    
    char         *errmsg;
    char         device_id[1024];  /*rpi#002*/
    char         serverip[1024];
    char	     db_file[1024]="./test.db";
    char         buf[1024];
    char 		conf_file;	
    struct sockaddr_in serv_addr;
    
    time_t		last_time;
    time_t		now_time;


    memset(buf, 0, sizeof(buf));
    memset(device_id, 0, sizeof(device_id));
    get_device_id(device_id,sizeof(device_id));
    iniparser_get(&conf_file,&port,serverip,sizeof(serverip));
    printf("serverip:%s,port:%d\n",serverip,port);    

      
    if (! serverip || !port)
    {
	    printf("error:ip and port get failed:%s\n",strerror(errno));
	    return -1;
    }

    if ((database_init(&db,db_file)) != 0)
    {
	    printf("open create table error\n");
	    return -2;
    }

    last_time = 0;


    while(!sig_stop)
    {	

        if (connfd < 0)
        {
            connfd = socket_connect(serverip,port);

            if ( connfd < 0 )
            {
                //printf("client connecte server failed\n");

            }
            else
            {
                sleep(1);
                //printf("client connecte server ok...\n");
            }
        }


        time(&now_time);

        if ((now_time - last_time) >= 5)
        {
            last_time = now_time;

            if ((ds18b20(&tmp)) != 0)
            {
                printf("get temperature\n");

            }

            if (connfd > 0)
            {
                rv = pack_json(buf, sizeof(buf), device_id, tmp);		
                if (rv < 0)
                {
                    printf("pack the data failed\n");
                    return -3;
                }

                if( write(connfd, buf, sizeof(buf)) < 0 )
                {
                    printf("Write data to server [%s:%d] failure: %s\n",serverip,port,strerror(errno));

                    printf ("connfd:%d\n",connfd);
                    if ((save_data(db,&tmp,device_id)) != 0)
                    {   
                        printf("save data to sqlite3 failed\n");
                        return -4;
                    }
                    
                    close(connfd);
                    connfd = -1;

                }

		    }


            if (connfd < 0)
            {   

                if ((save_data(db,&tmp,device_id)) != 0)
                {
                    printf("save data to sqlite3 failed\n");
                    return -5;
                }
            }

        }
          

  	    if (connfd > 0)
	    {
            if ((get_nrow(db,number)) != 0)
            {
                printf ("get the nrow ok\n");

            }
            else
            {
                printf("get the nrow ereor\n");
                sqlite3_close(db);
                close(connfd);
                break;

            }

            if (number == 0)
            {
                break;
                len = *number <= 10 ? *number : 10 ;
                for(i=1; i<len; i++)
                {

                    if ((get_data(db, &tmp, device_id)) != 0 )
                    {
                        printf("get data from sqlite3 ok\n");
                    }

                    rv = pack_json(buf, sizeof(buf), device_id, tmp);
                    if (rv < 0)
                    {
                        printf("pack the data failed\n");
                        return -6;
                    }

                    if( write(connfd, buf, sizeof(buf)) < 0 )
                    {
                        printf("Write data to server [%s:%d] failure: %s\n",serverip,port,strerror(errno));
                        close(connfd);
                        connfd = -1;
                        return -7;

                    }
                    else
                    {
                        //   printf("Write data to server ok\n");
                    }
                    if ((get_rowid(db)) != 0)
                    {
                        printf(" get rowid ok!\n");

                    }
                    printf ("rowid:%d\n",rowid);
                    if ((delete_data(db,rowid)) != 0)
                    {
                        printf("delete data success\n");
                        rowid++;
                    }
                    printf ("---------rowid:%d\n",rowid);

                }
            }
        }
    


        sqlite3_close(db);  
        close(connfd);
        connfd = -1;
    }
    
    return 0;

    
    
}

