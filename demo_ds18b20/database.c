#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sqlite3.h>
#include"database.h"


int database_init(sqlite3 **db,char* db_file)
{
	int	ret = -1;
	char*   errmsg;
	char   sql[1024];
	
	
	if ( !access(db_file, F_OK))
	{
		ret = sqlite3_open(db_file,db);
		if (ret != SQLITE_OK)
		{
			printf("open sqlfile error\n");
			return -2;
		}
//		printf("11111111111111\n");

	}
	else
	{
		ret = sqlite3_open(db_file,db);
		if (ret != SQLITE_OK)
		{	
			printf("create sqlfile error\n");
			return -1;
		}
	//	sql = "CREATE TABLE COMPANY(" \
		       "ID               TEXT        NOT NULL," \
		       "TMP              FLOAT      NOT NULL);";
               memset(sql, 0, sizeof(sql));
               snprintf(sql, sizeof(sql), "CREATE TABLE COMPANY(ID TEXT NOT NULL,TMP FLOAT NOT NULL);");
               ret = sqlite3_exec(*db, sql, NULL, NULL, &errmsg);		

               if (ret != SQLITE_OK)
               {
                   printf("open table company failed:%s\n",errmsg);
                   sqlite3_free(errmsg);
                   return -3;
               }
    }

    return 0;

}

int save_data(sqlite3 *db,float *tmp,char *device_id)
{
	int 	ret = -1;
	char*	errmsg;
	char	sql[1024];	


	memset(sql, 0, sizeof(sql));
//	snprintf(sql,sizeof(sql),"INSERT INTO COMPANY(ID, TMP) VALUES('%s',%.3f);", device_id, *tmp);
    snprintf(sql,sizeof(sql),"INSERT INTO COMPANY VALUES('%s',%.3f);", device_id, *tmp);
	
    ret = sqlite3_exec(db, sql, NULL, NULL, &errmsg);
    if ( ret != SQLITE_OK )
	{
		printf("save data to sqlite3 error:%s\n",errmsg);
        return -1;
	}
    printf("insert ok\n");
	


return 0;

}

int get_nrow(sqlite3 *db, int *number)
{
    int     ret = -1;
    char*   errmsg;
    int     nrow;
    char **result;
    char*   sql;

    sql = "select * from company;";

    ret = sqlite3_get_table(db, sql, &result, &nrow, NULL, &errmsg);

    if( ret != SQLITE_OK )
    {       
    //    fprintf(stderr, "SQL error: %s\n", errmsg);
        sqlite3_free(errmsg);
    }
    else
    {       
//        fprintf(stdout, "Operation done successfully\n");
    }
    
    *number = nrow;
    
  //  printf ("number:%d\n",*number);
    return 0;

}



int get_data(sqlite3 *db,float* tmp,char *device_id)
{       

	int     ret = -1;
	char*   errmsg;
	char*	sql;
    int     nrow;
    int     ncol;
    char **result;


	sql = "SELECT * from COMPANY limit 1";
    
	ret = sqlite3_get_table(db, sql, &result, &nrow, &ncol, &errmsg);

	if( ret != SQLITE_OK )
	{
		    fprintf(stderr, "SQL error: %s\n", errmsg);
		        sqlite3_free(errmsg);
	}
	else
	{
//		    fprintf(stdout, "get the data successfully\n");
	}
    
    device_id = (char *)result[ncol]; 
    ncol++;
    *tmp = atof((const char *)result[ncol]);

	return 0;
}

int get_rowid(sqlite3 *db)
{
    int     ret = -1;
    char*   errmsg;  
    char*   sql;
    
    sql = "select rowid, * from company;";
    
    if(sqlite3_exec(db, sql, NULL, NULL, &errmsg ) != SQLITE_OK)
    {                          
                                   
            
            printf("delete data error falure:%s\n", errmsg);

    }
    else
    {   
          //  printf("get the company rowid ok\n");
    }



    return 0;
}

int delete_data(sqlite3 *db,int rowid)
{
	int     ret = -1;
	char*   errmsg;
	char   sql[1024];
	memset(sql, 0, sizeof(sql));
	snprintf(sql, sizeof(sql), "delete from company where rowid=%d;", rowid);

	if(sqlite3_exec(db, sql, NULL, NULL, &errmsg ) != SQLITE_OK)
	{
		

		printf("delete data error falure:%s\n", errmsg);

	}
	else
	{
		printf("delete data ok\n");
	}

	return 0;
}
